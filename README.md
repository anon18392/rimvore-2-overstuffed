# RimVore 2 Overstuffed

A submod for Nabber's RimVore 2, which is a vore mod for Ludeon Studios' Rimworld.

This mod was created to handle the overflow from the main mod. There are just so many ways to do vore, and some people just aren't interested in them all.

Added vore types (entryways):
- Bladder: This is technically an extension of Cock and Vaginal vore.
- Cleavage
- Membrane: This is what slimes do! Comes with patches for all manner of slime race; if your slime isn't patched, we'll patch it!)
- Navel
- Tailmaw: A mouth-like hole in the tip of the tail
- Udder

Added vore goals (outcomes):
- Assimilation: Permanent absorption, analogous to digestion but with (planned) RJW part size improvements where applicable!
- Amalgamation: Temporary absorption, analogous to storage but with (planned) RJW part size improvements where applicable!
- Chemfuel conversion: Feed captives to your boomalopes and get chemfuel out of it!
- Egg conversion: Makes chicken eggs! (temporary implementation)
- Urine conversion: An alternative to the scat/bones resource for making fertilizer!
