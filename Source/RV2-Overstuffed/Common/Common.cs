﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RimVore2;
using Verse;

namespace RV2_Overstuffed
{
    public static class Common
    {
        public static VoreGoalDef assimilationGoalDef = DefDatabase<VoreGoalDef>.GetNamed("Assimilate");
        public static VoreGoalDef amalgamationGoalDef = DefDatabase<VoreGoalDef>.GetNamed("Amalgamate");
    }
}
