﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HarmonyLib;
using RimVore2;
using Verse;

namespace RV2_Overstuffed
{
    [HarmonyPatch(typeof(VoreValidator), "PredatorPassesVoreEnablerSelectors")]
    public class Patch_VoreTypeDef_
    {
        [HarmonyPrefix]
        public static void SkipTypeCheckForAssimilationAndAmalgamation(List<VoreTargetSelectorRequest> requests)
        {
            try
            {
                if (requests.Any(req => req.voreGoal == Common.assimilationGoalDef || req.voreGoal == Common.amalgamationGoalDef))
                {
                    //Log.Message("Intercepting requests for enablers: " + String.Join(", ", requests.Select(r => r.ToString())));
                    int count = requests.RemoveAll(request => request.voreType != null);
                    //Log.Message("Removed " + count + " requests invalidated by absorption");
                }
            }
            catch (Exception ex)
            {
                Log.Error("Something went wrong: " + ex);
            }
        }
    }
}
