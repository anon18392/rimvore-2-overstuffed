﻿using System.Reflection;
using HarmonyLib;
using Verse;

namespace RV2_Overstuffed
{
    [StaticConstructorOnStartup]
    public static class Startup
    {
        static Startup()
        {
            Harmony.DEBUG = false;
            Harmony harmony = new Harmony("rv2_os");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }
}
